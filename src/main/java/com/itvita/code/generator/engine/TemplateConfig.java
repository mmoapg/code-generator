package com.itvita.code.generator.engine;

import com.itvita.code.generator.entity.ColumnInfo;
import lombok.Data;

import java.util.List;

/**
 * @Time: 2021/1/22 11:23 上午
 * @author: liu.q [916000612@qq.com]
 * @des: 模板配置
 */
@Data
public class TemplateConfig {
    /*列信息*/
    private List<ColumnInfo> columnInfoList;
    /*包名*/
    private String packageName;
    /*表名*/
    private String tableName;
    /*entity名大写*/
    private String upperEntityName;
    /*entity名小写*/
    private String lowerEntityName;
    /*日期*/
    private String dateTime;
    /*作者*/
    private String author;
    /*备注*/
    private String desc;
    /*映射地址*/
    private String requestMapping;
    /*权限标识*/
    private String authSite;

}

package com.itvita.code.generator.service;

import com.itvita.code.generator.utils.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.StringTemplateResourceLoader;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Slf4j
public class PageService {
    private String projectPath; //跟路径
    private String modelName; //模块名称
    private String author; //开发者
    private String tableName; //表名
    private String requestMapping; //跟映射

    /*apiJs模板路径*/
    private final static String apiJSTemplatePath = "templates/api.js.btl";
    private GroupTemplate groupTemplate;

    /**
     * @Time: 2021/1/29 16:56
     * @author: liu.q [916000612@qq.com]
     * @des:  初始化基础参数
     * */
    public PageService init(String projectPath, String modelName, String author, String tableName, String requestMapping) throws IOException {
        this.projectPath = projectPath;
        this.modelName = modelName;
        this.author = author;
        this.tableName = tableName;
        this.requestMapping = requestMapping;
        //初始化代码
//        ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader("templates");
//        Configuration cfg = Configuration.defaultConfiguration();
//        this.groupTemplate = new GroupTemplate(resourceLoader, cfg);
        //初始化代码
        StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
        Configuration cfg = Configuration.defaultConfiguration();
        groupTemplate = new GroupTemplate(resourceLoader, cfg);

        return this;
    }

    /**
     * @Time: 2021/1/29 16:56
     * @author: liu.q [916000612@qq.com]
     * @des:  创建目录
     * */
    public String mkdirs(String staticPath) {
        String s = projectPath.substring(projectPath.length() - 1);
        if (s.equals(File.separator)) {
            projectPath = projectPath.substring(0, projectPath.length() - 1);
        }
        String path = projectPath + File.separator + modelName + File.separator + staticPath;

        File dir = new File(path);
        if (!dir.exists()) {
            boolean result = dir.mkdirs();
            if (result) {
                log.debug("创建目录： [" + path + "]");
            }
        }
        return path;
    }

    /**
     * @Time: 2021/1/29 16:56
     * @author: liu.q [916000612@qq.com]
     * @des:  api-js
     * */
    public PageService execApiJs() throws IOException {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        InputStream in = resourceLoader.getResource(apiJSTemplatePath).getInputStream();
        String apiJsStr = IOUtils.toString(in, "utf-8");
        Template t = groupTemplate.getTemplate(apiJsStr);
        t.binding("author", author);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        t.binding("time", dtf.format(LocalDateTime.now()));
        /*请求映射地址*/
        t.binding("action", getAction());
        String str = t.render();
        String staticPath = "src" + File.separator + "api" + File.separator + "server";
        String filePath = mkdirs(staticPath) + File.separator + CommonUtil.line2Hump(tableName) + ".js";
        File file = new File(filePath);
        OutputStream outputStream = new FileOutputStream(file);
        outputStream.write(str.getBytes());
        outputStream.close();
        return this;
    }

    private String getAction() {
        String s = requestMapping.substring(requestMapping.length() - 1);
        if (s.equals("/")) {
            return requestMapping + tableName.toLowerCase(Locale.CHINA).replaceAll("_", "/");
        } else {
            return requestMapping + "/" + tableName.toLowerCase(Locale.CHINA).replaceAll("_", "/");
        }
    }

    /**
     * @Time: 2021/1/29 16:56
     * @author: liu.q [916000612@qq.com]
     * @des:  编辑页
     * */
    public PageService execEditVue(String editVue) throws IOException {
        Template t = groupTemplate.getTemplate(editVue);
        t.binding("author", author);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        t.binding("time", dtf.format(LocalDateTime.now()));
        t.binding("jsApiName",CommonUtil.line2Hump(tableName) + ".js");
        String str = t.render();
        String staticPath = "src" + File.separator + "views" + File.separator + "server" + File.separator + CommonUtil.line2Hump(tableName);
        String filePath = mkdirs(staticPath) + File.separator + "edit.vue";
        File file = new File(filePath);
        OutputStream outputStream = new FileOutputStream(file);
        outputStream.write(str.getBytes());
        outputStream.close();
        return this;
    }

    /**
     * @Time: 2021/1/29 16:56
     * @author: liu.q [916000612@qq.com]
     * @des:  列表页
     * */
    public PageService execIndexVue(String indexVue) throws IOException {
        Template t = groupTemplate.getTemplate(indexVue);
        t.binding("author", author);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        t.binding("time", dtf.format(LocalDateTime.now()));
        t.binding("jsApiName",CommonUtil.line2Hump(tableName) + ".js");
        /*请求映射地址*/
        t.binding("action", getAction());
        String str = t.render();
        String staticPath = "src" + File.separator + "views" + File.separator + "server" + File.separator + CommonUtil.line2Hump(tableName);
        String filePath = mkdirs(staticPath) + File.separator + "index.vue";
        File file = new File(filePath);
        OutputStream outputStream = new FileOutputStream(file);
        outputStream.write(str.getBytes());
        outputStream.close();
        return this;
    }

    /**
     * @Time: 2021/1/29 16:55
     * @author: liu.q [916000612@qq.com]
     * @des:  详情页
     * */
    public PageService execDetailVue(String detailVue) throws IOException {
        Template t = groupTemplate.getTemplate(detailVue);
        t.binding("author", author);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        t.binding("time", dtf.format(LocalDateTime.now()));
        t.binding("jsApiName",CommonUtil.line2Hump(tableName) + ".js");
        String str = t.render();
        String staticPath = "src" + File.separator + "views" + File.separator + "server" + File.separator + CommonUtil.line2Hump(tableName);
        String filePath = mkdirs(staticPath) + File.separator + "detail.vue";
        File file = new File(filePath);
        OutputStream outputStream = new FileOutputStream(file);
        outputStream.write(str.getBytes());
        outputStream.close();
        return this;
    }
}

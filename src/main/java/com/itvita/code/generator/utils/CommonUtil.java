package com.itvita.code.generator.utils;

import com.itvita.code.generator.constant.DbColumnType;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtil {

    /**
     * @Time: 2021/1/22 11:45 上午
     * @author: liu.q [916000612@qq.com]
     * @des: 数据库类型 --> java类型转换
     */

    public static DbColumnType processTypeConvert(String dataType) {

        List<String> stringArray = new ArrayList<String>() {{
            add("char");
            add("text");
            add("longtext");
            add("json");
            add("varchar");
        }};

        List<String> integerArray = new ArrayList<String>() {{
            add("int");
            add("integer");
            add("smallint");
        }};

        if (stringArray.indexOf(dataType) > -1) {
            return DbColumnType.STRING;
        } else if ("bigint".equals(dataType)) {
            return DbColumnType.LONG;
        } else if (integerArray.indexOf(dataType) > -1) {
            return DbColumnType.INTEGER;
        } else if ("decimal".equals(dataType)) {
            return DbColumnType.BIG_DECIMAL;
        } else if ("double".equals(dataType)) {
            return DbColumnType.DOUBLE;
        } else if ("datetime".equals(dataType)) {
            return DbColumnType.LOCAL_DATE_TIME;
        } else if ("date".equals(dataType)) {
            return DbColumnType.LOCAL_DATE;
        } else if ("time".equals(dataType)) {
            return DbColumnType.LOCAL_TIME;
        } else {
            return DbColumnType.STRING;
        }
    }

    /**
     * @Time: 2021/1/22 2:28 下午
     * @author: liu.q [916000612@qq.com]
     * @des:  下划线转驼峰
     * */
    private static Pattern linePattern = Pattern.compile("_(\\w)");
    public static String line2Hump(String str) {
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * @Time: 2021/1/22 2:33 下午
     * @author: liu.q [916000612@qq.com]
     * @des:  驼峰转下划线
     * */
    private static Pattern humpPattern = Pattern.compile("[A-Z]");
    public static String hump2Line(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}

package com.itvita.code.generator.entity;

import lombok.Data;

/**
 * @Time: 2021/1/19 4:29 下午
 * @author: liu.q [916000612@qq.com]
 * @des:
 * */
@Data
public class TableInfo {
    /*表名*/
    private String tableName;
    /*表备注*/
    private String tableComment;
}

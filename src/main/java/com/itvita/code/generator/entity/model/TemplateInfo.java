package com.itvita.code.generator.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor                 //无参构造
@AllArgsConstructor                //有参构造
public class TemplateInfo {
    /*模板名称*/
    private String name;
    /*模块名*/
    private String moduleName;
    /*固定路径*/
    private String staticPath;
    /*默认包*/
    private String rootPackageName;
    /*包名*/
    private String packageName;
    /*是否覆盖 1是，0否*/
    private Integer isCover = 0;
}

package com.itvita.code.generator.entity;

import cn.hutool.core.util.StrUtil;
import com.itvita.code.generator.utils.CommonUtil;
import lombok.Data;

/**
 * @Time: 2021/1/22 9:35 上午
 * @author: liu.q [916000612@qq.com]
 * @des: 表字段信息
 */
@Data
public class ColumnInfo {

    /*列名*/
    private String columnName;
    /*列备注*/
    private String columnComment;
    /*列类型*/
    private String dataType;
    /*java 数据类型*/
    private String javaType;

    /*列名*/
    private String propertyName;

    public String getJavaType() {
        return CommonUtil.processTypeConvert(dataType).getType();
    }

    public String getPropertyName() {
        return StrUtil.lowerFirst(CommonUtil.line2Hump(columnName));
    }

}

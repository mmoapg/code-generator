package com.itvita.code.generator.entity.model;

import lombok.Data;

import java.util.List;

/**
 * @Time: 2021/1/22 9:32 上午
 * @author: liu.q [916000612@qq.com]
 * @des: 服务端代码生成参数
 */
@Data
public class CreateInfo {
    /*表*/
    private List<String> tables;
    /*开发者*/
    private String author;
    /*模板配置*/
//    private JSONObject templates;
    /*数据源*/
    private DataSource dataSource;
    /*项目跟路径*/
    private String projectPath;
    /*包名*/
    private String packageName;
    /*映射跟地址*/
    private String contextPath;
}

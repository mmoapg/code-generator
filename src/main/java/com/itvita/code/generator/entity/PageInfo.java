package com.itvita.code.generator.entity;

import lombok.Data;

@Data
public class PageInfo {
    public String packageName;
    public String bigEntityName;
    public String lowerEntityName;
}

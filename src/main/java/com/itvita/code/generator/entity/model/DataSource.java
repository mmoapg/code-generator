package com.itvita.code.generator.entity.model;

import lombok.Data;

/**
 * @Time: 2021/1/22 9:34 上午
 * @author: liu.q [916000612@qq.com]
 * @des:  数据源信息
 * */
@Data
public class DataSource {
    /*数据库类型*/
    private String type = "MYSQL";
    /*主机地址*/
    private String host;
    /*用户名*/
    private String userName;
    /*密码*/
    private String password;
    /*端口号*/
    private String port;
    /*数据库*/
    private String database;
}

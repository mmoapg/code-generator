package com.itvita.code.generator.entity.vo;

import lombok.Data;

@Data
public class ReturnVo<T> {
    private Integer code;
    private String msg;
    private T data;

    public ReturnVo() {
        this.code = 1;
    }

    public ReturnVo(Vo vo) {
        this.code = vo.getCode();
        this.msg = vo.getMsg();
    }

    public ReturnVo(Vo vo,String msg) {
        this.code = vo.getCode();
        this.msg = msg;
    }

    public ReturnVo(T data) {
        Vo vo = Vo.OK;
        this.code = vo.getCode();
        this.msg = vo.getMsg();
        this.data = data;
    }
}

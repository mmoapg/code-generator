package com.itvita.code.generator.entity.vo;

import lombok.Getter;

@Getter
public enum Vo {
    OK(1, "请求成功"),
    ERROR(0,"请求错误"),
    LOGIN_TIMEOUT(-1, "登录超时"),//登录超时，触发登录
    LOGIN_EXPIRED(-2, "登录过期"), //登录过期，不触发登录
    NO_PERMISSION(403,"缺少权限"),
    NO_LOGIN(-100,"未登录"),
    Exception(500,"服务异常");

    private Integer code;
    private String msg;

    Vo(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

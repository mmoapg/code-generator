package com.itvita.code.generator.constant;

import java.io.File;

/**
 * @Time: 2021/1/21 4:21 下午
 * @author: liu.q [916000612@qq.com]
 * @des: 模板静态常量
 */
public class TemplateConstVal {

    /*static*/
    public final static String TEMPLATE_JAVA_STATICPATH = File.separator + "src" + File.separator + "main" + File.separator + "java";
    public final static String TEMPLATE_XML_STATICPATH = File.separator + "src" + File.separator + "main" + File.separator + "resources";
    /*模板路径*/
    public final static String TEMPLATE_ENTITY_JAVA_NAME_PATH = "entity.java.btl";
    public final static String TEMPLATE_ENTITYDTO_JAVA_NAME_PATH = "entityDto.java.btl";
    public final static String TEMPLATE_MAPPER_NAME_PATH = "mapper.java.btl";
    public final static String TEMPLATE_XML_NAME_PATH = "mapper.xml.btl";
    public final static String TEMPLATE_SERVICE_NAME_PATH = "service.java.btl";
    public final static String TEMPLATE_SERVICE_IMPL_NAME_PATH = "serviceImpl.java.btl";
    public final static String TEMPLATE_CONTROLLER_NAME_PATH = "controller.java.btl";

}

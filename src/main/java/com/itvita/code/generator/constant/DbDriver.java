package com.itvita.code.generator.constant;
/**
 * @Time: 2021/1/19 3:50 下午
 * @author: liu.q [916000612@qq.com]
 * @des: 数据库驱动
 */

public enum DbDriver {
    MYSQL("com.mysql.cj.jdbc.Driver"
            , "jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=utf8&useSSL=true&serverTimezone=Asia/Shanghai");


    private String driver;
    private String url;

    DbDriver(String driver, String url) {
        this.driver = driver;
        this.url = url;
    }

    public String getDriver() {
        return driver;
    }

    public String getUrl(String host,String port,String database) {
        return String.format(this.url,host,port,database);
    }
}

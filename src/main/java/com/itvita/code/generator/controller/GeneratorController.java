package com.itvita.code.generator.controller;

import com.alibaba.fastjson.JSONObject;
import com.itvita.code.generator.config.JdbcTemplateConfig;
import com.itvita.code.generator.engine.AbstractTemplateEngine;
import com.itvita.code.generator.engine.BeetlTemplateEngine;
import com.itvita.code.generator.entity.ColumnInfo;
import com.itvita.code.generator.entity.TableInfo;
import com.itvita.code.generator.entity.model.CreateInfo;
import com.itvita.code.generator.entity.model.DataSource;
import com.itvita.code.generator.entity.vo.ReturnVo;
import com.itvita.code.generator.entity.vo.Vo;
import com.itvita.code.generator.service.GeneratorService;
import com.itvita.code.generator.service.PageService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.List;

@RestController
@RequestMapping("/code/generator")
public class GeneratorController {

    /**
     * @Time: 2021/1/22 10:55 上午
     * @author: liu.q [916000612@qq.com]
     * @des: 测试链接
     */
    @GetMapping("/testConnection")
    public ReturnVo testConnection(DataSource dataSource) throws Exception {
        String s = JdbcTemplateConfig.testConnection(dataSource);
        if ("OK".equals(s)) {
            return new ReturnVo(s);
        } else {
            return new ReturnVo(Vo.ERROR, s);
        }
    }

    

    /**
     * @Time: 2021/1/22 10:55 上午
     * @author: liu.q [916000612@qq.com]
     * @des: 获取数据库表信息
     */
    @GetMapping("/getTableInfos")
    public ReturnVo<List<TableInfo>> getTableInfos(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = JdbcTemplateConfig.getJdbcTemplate(dataSource);
        GeneratorService generatorService = new GeneratorService(jdbcTemplate);
        List<TableInfo> list = generatorService.getTableInfo(dataSource.getDatabase());
        return new ReturnVo(list);
    }

    /**
     * @Time: 2021/1/22 10:55 上午
     * @author: liu.q [916000612@qq.com]
     * @des: 本地生成代码
     */
    @PostMapping("/createLocal")
    public ReturnVo createLocal(@RequestBody CreateInfo createInfo) {
        AbstractTemplateEngine beetlTemplateEngine = new BeetlTemplateEngine();
        beetlTemplateEngine.init(createInfo).exec();
        return new ReturnVo();
    }

    /**
     * @Time: 2021/1/22 10:55 上午
     * @author: liu.q [916000612@qq.com]
     * @des: 本地前端代码生成
     */
    @PostMapping("/createLocalPage")
    public ReturnVo createLocalPage(@RequestBody JSONObject param) throws IOException {
        /*详情配置*/
        String detailVue = param.getString("detailVue");
        /*编辑页配置*/
        String editVue = param.getString("editVue");
        /*表格配置*/
        String indexVue = param.getString("indexVue");
        /*项目路径*/
        String projectPath = param.getString("projectPath");
        /*模块名称*/
        String modelName = param.getString("modelName");
        /*表名*/
        String tableName = param.getString("tableName");
        /*映射地址*/
        String requestMapping = param.getString("requestMapping");
        /*开发者*/
        String author = param.getString("author");
        new PageService()
                .init(projectPath, modelName, author, tableName, requestMapping)
                .execApiJs()
                .execEditVue(editVue)
                .execDetailVue(detailVue)
                .execIndexVue(indexVue);
        return new ReturnVo();
    }

    /**
     * @Time: 2021/1/22 10:55 上午
     * @author: liu.q [916000612@qq.com]
     * @des: 获取列信息
     */
    @GetMapping("/getColumnInfos")
    public ReturnVo<List<ColumnInfo>> getColumnInfos(DataSource dataSource, String tableName) {
        JdbcTemplate jdbcTemplate = JdbcTemplateConfig.getJdbcTemplate(dataSource);
        GeneratorService generatorService = new GeneratorService(jdbcTemplate);
        List<ColumnInfo> list = generatorService.getColumnInfo(tableName);
        return new ReturnVo(list);
    }

    public static void main(String[] args) throws Exception {
        DataSource dataSource = new DataSource();
        dataSource.setHost("127.0.0.1");
        dataSource.setUserName("root");
        dataSource.setPassword("123456");
        dataSource.setPort("3306");
        dataSource.setDatabase("blog");

//        GeneratorService generatorService = new ServerUtil<GeneratorService>().getService(dataSource,GeneratorService.class);
//        List<Table> list = generatorService.getTables(dataSource.getDatabase());

        JdbcTemplate jdbcTemplate = JdbcTemplateConfig.getJdbcTemplate(dataSource);
        Constructor<GeneratorService> constructor = GeneratorService.class.getConstructor();
        GeneratorService generatorService = constructor.newInstance(jdbcTemplate);

        System.out.println(generatorService);
//        JdbcTemplate jdbcTemplate = JdbcTemplateConfig.getJdbcTemplate(dataSource);
//        GeneratorService generatorService = new GeneratorService(jdbcTemplate);
//        List<Table> list = generatorService.getTables(dataSource.getDatabase());
//        System.out.println(list);


    }
}

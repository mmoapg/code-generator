package com.itvita.code.generator.controller;

import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 测试
 *
 * @Time: 2021/9/29 11:48 上午
 * @author: liu.q [916000612@qq.com]
 */
@RestController
public class TestController {

    @GetMapping("/test/table")
    public ReturnVo test() {

        List list = new ArrayList();
        for (int i = 0; i < 10; i++) {
            Map m = new HashMap<>();
            m.put("id", i + 10000);
            m.put("name", "test" + i);
            m.put("age", i + 20);
            m.put("remark", "备注" + i);
            list.add(m);
        }

        Map result = new HashMap();
        result.put("rows", list);
        result.put("pageNumber", 1);
        result.put("pageSize", 10);
        result.put("pages", 100);
        result.put("size", 10);
        result.put("total", 100);


        return new ReturnVo(result);
    }

    @Data
    class ReturnVo {
        private Integer code;
        private String msg;
        private Object data;

        public ReturnVo(Object data) {
            this.data = data;
            this.code = 1;
            this.msg = "操作成功";
        }
    }
}

package com.itvita.code.generator.config;

import com.itvita.code.generator.entity.vo.ReturnVo;
import com.itvita.code.generator.entity.vo.Vo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 处理异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ReturnVo exceptionHandler(Exception e) {
        log.error("异常：", e);
        return new ReturnVo(Vo.Exception, e.getMessage());
    }
}

package com.itvita.code.generator.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Time: 2020/11/20 2:35 下午
 * @author: liu.q [916000612@qq.com]
 * @des:  解决跨域
 * */
@Component
@Slf4j
public class CorsFilter implements Filter{
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest reqs = (HttpServletRequest) req;
        response.setHeader("Access-Control-Allow-Origin",reqs.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "*");

//        response.setHeader("Access-Control-Allow-Headers", "x-requested-with,brand-user-token-admin");
        chain.doFilter(req, res);
    }
    public void init(FilterConfig filterConfig) {
        log.info("cors过滤器初始化");
    }
    public void destroy() {
        log.info("cors过滤器销毁");
    }
}

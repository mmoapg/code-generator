package com.itvita.code.generator.mapper;

import com.itvita.code.generator.entity.ColumnInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ColumnInfoMapper implements RowMapper<ColumnInfo> {

    @Override
    public ColumnInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        String columnName = resultSet.getString("columnName");
        String columnComment = resultSet.getString("columnComment");
        String dataType = resultSet.getString("dataType");

//        把数据封装成User对象
        ColumnInfo columnInfo = new ColumnInfo();
        columnInfo.setColumnName(columnName);
        columnInfo.setColumnComment(columnComment);
        columnInfo.setDataType(dataType);
        return columnInfo;
    }
}

package com.itvita.code.generator.mapper;

import com.itvita.code.generator.entity.TableInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
public class TableInfoMapper implements RowMapper<TableInfo> {

    @Override
    public TableInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        String table_name = resultSet.getString("table_name");
        String table_comment = resultSet.getString("table_comment");
//        把数据封装成User对象
        TableInfo table = new TableInfo();
        table.setTableName(table_name);
        table.setTableComment(table_comment);
        return table;
    }
}
